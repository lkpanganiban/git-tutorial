# Git Tutorial
## What is Git?
Git is a distributed version control system which allows you to track the changes that you or your members made to a certain file.  Git was initially designed and developed in 2005 by Linux kernel developers (including Linus Torvalds) for Linux kernel development.
## Download and Install Git
  - Windows refer to this link: <http://git-scm.com/download/win> 
  - Mac OSX refer to this link: <http://git-scm.com/download/mac>
  - Linux - Ubuntu: sudo apt-get install git-all

## Main Git Commands
### Initial Setup
```sh
$ git config --global user.name "DA PRDP"
$ git config --global user.email "da-prdp@gmail.com"
```

### Checking the Proxy Settings
```sh
$ git config --global --get http.proxy
$ git config --global --get https.proxy
```

### Setting up Git with Proxy Servers
```sh
$ git config --global http.proxy http://proxyuser:proxypwd@proxy.server.com:8080
$ git config --global http.proxy https://proxyuser:proxypwd@proxy.server.com:8080
```

### Unsetting the Proxy Settings
```sh
$ git config --global --unset http.proxy
$ git config --global --unset https.proxy
```

### Initializing a Repository
The repository of your code must first be initialized into a git repo before git can track the changes that you've made.
```sh
$ git init
```

### Tracking your Files
Git add updates the index using the current content found in the working tree, to prepare the content staged for the next commit
```sh
$ git add <filename> or --all 
```

### Making your First Commit
Stores the current contents of the index in a new commit along with a log message from the user describing the changes into git's own database or log file.
```sh
$ git commit -m "My Commit Message" 
```

### Creating a Feature Branch
Creates a different branch of your code. Useful when doing exploratory coding when you don't want to affect the current/stable codebase. 
```sh
$ git checkout -b <branch-name> 
```

### Switching to a Feature Branch or Reverting to an Old Commit
```sh
$ git checkout <branch-name> or $ git checkout <hash-name>
```

### Merging a Feature Branch
If you want your exploratory branch to be added to the main branch you will need to merge them.
```sh
$ git checkout <name-of-main-branch>
$ git merge <name-of-branch-to-be-merged>
```

### Setting up a Remote Repository
Git commits is only stored in your local machine. Let's say you want to contribute to a remote repository. Then we must first establish a link to that remote repo.
```sh
$ git remote add <remote-name> <url> 
```

### Switching a Remote Repository
If you want to change the remote url you can do the following:
```sh
$ git remote set-url <remote-name> <url> 
```

### Contributing to a Remote Repository
The following command allows you to add/upload your changes to the main code repository.
```sh
$ git push <remote-name> <branch-name>
```

### Updating your Working Repository
Most of the time there are changes that have been uploaded to a remote repository therefore you need to update your local copy of the repo to avoid conflicts.
```sh
$ git pull <remote-name> <branch-name>
```
## Other Useful Git Commands
This shows the status of your git workflow.
```sh
$ git status
```
<br/>
This shows the git logs (the logs of your commits or contribution logs).
```sh
$ git logs
```

<br/>
This shows the different things that you've changed in your code.
```sh
$ git diff
```
